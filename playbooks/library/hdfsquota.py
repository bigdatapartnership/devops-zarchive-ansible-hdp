#!/usr/bin/env python

################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################

import sys
import json
import os
import shlex
import subprocess

args_file = sys.argv[1]
args_data = file(args_file).read()
arguments = shlex.split(args_data)

if len(arguments) < 1:
    print json.dumps({
        "failed": True,
        "msg": "Missing ARGS"
    })
    sys.exit(1)

path = None
spacequota = "50g"
filequota = 10000

def setfileqouta(path, filequota):
	fscommand = "sudo su -l hdfs -c \"hadoop dfsadmin -setQuota {0} {1} \"".format(filequota, path) 
	call_params = shlex.split(fscommand)
	p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
	output, err = p.communicate()
	return p.returncode, err


def setspaceqouta(path, filequota):
	fscommand = "sudo su -l hdfs -c \"hadoop dfsadmin -setSpaceQuota {0} {1} \"".format(spacequota, path)
	call_params = shlex.split(fscommand)
	p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
	output, err = p.communicate()
	return p.returncode

for arg in arguments:
    if "=" in arg:
        (key, value) = arg.split("=")
        if key == "spacequota":
            spacequota = value
        elif key == "filequota":
            filequota = value
        elif key == "path":
        	path = value

if spacequota is None:
    print json.dumps({
        "failed": False,
        "msg": "Spacequota not provided. Using Default 50g"
    })

if filequota is None:
    print json.dumps({
        "failed": False,
        "msg": "filequota not provided. Using Default 10000"
    })

filequota_status ,err = setfileqouta(path, filequota)
if(filequota_status != 0):
	print json.dumps({
		"failed": True,
		"msg": "Issue while setting File Quota. Error: "+err
		})
	sys.exit(1)

spacequota_status, err = setspaceqouta(path, spacequota)
if(spacequota_status != 0):
	print json.dumps({
		"failed": True,
		"msg": "Issue while setting Space  Quota. Error: "+err
		})
	sys.exit(1)

print json.dumps({
	"result": "OK",
	"file quota": filequota,
	"space quota": spacequota,
	"changed": True
	})

