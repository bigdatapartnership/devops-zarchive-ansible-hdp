#!/usr/bin/env python

################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################

import sys
import json
import os
import shlex
import subprocess
import os.path

args_file = sys.argv[1]
args_data = file(args_file).read()
arguments = shlex.split(args_data)

if len(arguments) < 2:
    print json.dumps({
        "failed": True,
        "msg": "Missing ARGS"
    })
    sys.exit(1)

src = None
dest = None
user = None

def checkdir(dest):
    fscommand = 'hadoop dfs -test -d ' + dest
    call_params = shlex.split(fscommand)
    p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
    output, err = p.communicate()
    return p.returncode

def checkfile(dest):
    fscommand = 'hadoop dfs -test -e ' + dest
    call_params = shlex.split(fscommand)
    p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
    output, err = p.communicate()
    return p.returncode

def copy_from_local(src, dest):
    fscommand = 'sudo su -l ' + user + ' -c \"hadoop fs -copyFromLocal ' + src + ' ' + dest + '\"'
    call_params = shlex.split(fscommand)
    p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
    output, err = p.communicate()
    return p.returncode ,err

for arg in arguments:
    if "=" in arg:
        (key, value) = arg.split("=")
        if key == "src":
            src = value
        elif key == "dest":
            dest = value
        elif key == "user":
            user = value
        
if src is None:
    print json.dumps({
        "failed": True,
        "msg": "Source Path argument Missing."
    })
    sys.exit(1)

if dest is None:
    print json.dumps({
        "failed": True,
        "msg": "Destination path argument is  Missing"
    })
    sys.exit(1)

if user is None:
    print json.dumps({
        "failed": True,
        "msg": "User is  Missing"
    })
    sys.exit(1)

dest_status = checkdir(dest)
if(dest_status != 0):
    dest_status = checkfile(dest)
    if(dest_status !=0):
        copy_status, err = copy_from_local(src, dest)
        if(copy_status != 0):
            print json.dumps({
                "failed": True,
                "msg": "Issue while copying file" + err
                })
            sys.exit(1)
        else:
            print json.dumps({
	             "result": "OK",
	             "Source": src,
	             "Dest": dest,
	             "changed": True
	            })
    else:
        print json.dumps({
            "msg": "File already exist. Skipping copy"
            })
else:
    print json.dumps({
        "msg": "Directory already exist. Skipping copy"
        })
