#!/usr/bin/env python

################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################

import sys
import json
import os
import shlex
import subprocess

args_file = sys.argv[1]
args_data = file(args_file).read()
arguments = shlex.split(args_data)

if len(arguments) < 4:
    print json.dumps({
        "failed": True,
        "msg": "Missing ARGS" + sys.argv[1]
    })
    sys.exit(1)

path = None
user = None
group = None
permission = None

def checkdir(path):
    fscommand = 'hadoop fs -ls ' + path
    call_params = shlex.split(fscommand)
    p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
    output, err = p.communicate()
    return p.returncode


def createdir(path):
    fscommand = 'sudo su -l hdfs -c \"hadoop fs -mkdir ' + path + '\"'
    call_params = shlex.split(fscommand)
    p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
    output, err = p.communicate()
    return p.returncode, err

def chown(user , group , path):
	fscommand = 'sudo su -l hdfs -c \"hadoop fs -chown -R ' + user +':' + group + ' ' + path + '\"'
	call_params = shlex.split(fscommand)
	p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
	output, err = p.communicate()
	return p.returncode, err

def chmod(path, permission):
	fscommand = 'sudo su -l hdfs -c \"hadoop fs -chmod -R ' + permission + ' ' + path + '\"'
	call_params = shlex.split(fscommand)
	p = subprocess.Popen(call_params, stdout=subprocess.PIPE)
	output, err = p.communicate()
	return p.returncode, err

for arg in arguments:
    if "=" in arg:
        (key, value) = arg.split("=")
        if key == "path":
            path = value
        elif key == "user":
            user = value
        elif key == "group":
            group = value
        elif key == "permission":
            permission = value

if path is None:
    print json.dumps({
        "failed": True,
        "msg": "Directory Path Missing"
    })
    sys.exit(1)

if user is None:
    print json.dumps({
        "failed": True,
        "msg": "Owner user is  Missing"
    })
    sys.exit(1)

if group is None:
    print json.dumps({
        "failed": True,
        "msg": "group is  Missing"
    })
    sys.exit(1)

if permission is None:
    print json.dumps({
        "failed": True,
        "msg": "Directory permission are  Missing"
    })
    sys.exit(1)

dir_status = checkdir(path)
if(dir_status != 0):
    dir_create, err = createdir(path)
    if(dir_create != 0):
        print json.dumps({
            "failed": True,
            "msg": "Directory creation failed. Error : "+err
           	})
        sys.exit(1)

chown_status, err = chown(user, group, path)
if(chown_status != 0 ):
	print json.dumps({
		"failed": True,
		"msg": "Issue while changing ownership. Error: "+err
		})
	sys.exit(1)

chmod_status, err = chmod(path, permission)
if(chmod_status != 0):
	print json.dumps({
		"failed": True,
		"msg": "Issue while changing permission. Error: "+err
		})
	sys.exit(1)

print json.dumps({
	"result": "OK",
	"directory": path,
	"owner": user,
	"group": group,
	"permission": permission,
	"changed": True
	})
