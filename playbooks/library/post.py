#!/usr/bin/env python

################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################


import requests
import sys
import json
import os
import shlex

args_file = sys.argv[1]
args_data = file(args_file).read()
arguments = shlex.split(args_data)

if len(arguments) < 2:
    print json.dumps({
        "failed": True,
        "msg": "Missing ARGS"
    })
    sys.exit(1)

url = None
user = None
password = None
filename = None
headers = None
debug = False

for arg in arguments:
    if "=" in arg:
        (key, value) = arg.split("=")
        if key == "url":
            url = value
        elif key == "user":
            user = value
        elif key == "password":
            password = value
        elif key == "filename":
            filename = value
        elif key == "headers":
            headers = value
        elif key == "debug":
            debug = True

if url is None:
    print json.dumps({
        "failed": True,
        "msg": "URL Missing"
    })
    sys.exit(1)

if filename is None:
    print json.dumps({
        "failed": True,
        "msg": "Filename Missing"
    })
    sys.exit(1)

try:
    f = open(filename, 'r')
    data = f.read()
    f.close()
except Exception as e:
    error_message = "Error opening file"
    print json.dumps({
        "failed": True,
        "msg": error_message
    })
    sys.exit(1)

if debug:
    print("Url: " + url)
    print("User: " + user)
    print("Password: " + password)
    print("Header: " + headers)
    print("Filename: " + filename)

if user is not None and password is not None:
    if headers is not None:
        headers_convert = headers.replace("'", "\"")
        header = json.loads(headers_convert)
        r = requests.post(url, data, auth=(user, password), headers=header)
    else:
        r = requests.post(url, data, auth=(user, password))
else:
    if headers is not None:
        headers_convert = headers.replace("'", "\"")
        header = json.loads(headers_convert)
        r = requests.post(url, data, headers=header)
    else:
        r = requests.post(url, data)

if debug:
    r.__dict__

if r.status_code in [200, 201, 202]:
    print json.dumps({
        "result": "Post Ok",
        "changed": True
    })
else:
    error_message = "Error from web server: " \
                    + str(r.status_code) + " " + r.text
    print json.dumps({
        "failed": True,
        "msg": error_message
        })
    sys.exit(1)
