# Big Data Partnership - Hortonworks Ansible setup

This set of Ansible scripts has been designed to be run against a set of configured CentOS 6 servers, these can either be the Big Data Partnership docker containers or can be real / cloud servers running the latest updates to CentOS 6.

For the setup to work please update the hosts file with the correct IP Addresses to access the server and ensure the vars.yml file also has the correct details for your environment. Once that is done you can use the make commands to install a HDP cluster. Initially without a blueprint supplied it will install Ambari Server on the master node and Ambari Clients on the other nodes ready for you to continue the setup of Hortonworks via Ambari. If you have a working blueprint this can be supplied in which case these scripts will complete the install by posting the blueprint to the Ambari master.


    make provision



# Big Data Partnership - Hortonworks Hue  Ansible setup

This set of Ansible scripts has been designed to be run against a set of configured CentOS 6 servers, these can either be the Big Data Partnership docker containers or can be real / cloud servers running the latest updates to CentOS 6.

For the setup to work please update the hosts file with the correct IP Addresses to access the server and ensure the vars.yml file also has the correct details for your environment.


Before running make to install either environment you need to make some manual changes within Ambari.

1. Stop namenode

2. Update the Hadoop environment settings

a. Edit hdfs-site.xml

    <property>
          <name>dfs.webhdfs.enabled</name>
          <value>true</value>
    </property>

b. Edit core-site.xml 

    <property>
      <name>hadoop.proxyuser.hue.hosts</name>
      <value>*</value>
    </property>
                  
    <property>
      <name>hadoop.proxyuser.hue.groups</name>
      <value>*</value>
    </property>
                  
    <property>
      <name>hadoop.proxyuser.hcat.groups</name>
      <value>*</value>
    </property>
                  
    <property>
      <name>hadoop.proxyuser.hcat.hosts</name>
      <value>*</value>
    </property>


3. Update WebHCAT settings.

a. Edit webhcat-site.xml

    <property>
      <name>webhcat.proxyuser.hue.hosts</name>
      <value>*</value>
    </property>
                  
    <property>
      <name>webhcat.proxyuser.hue.groups</name>
      <value>*</value>
    </property>


4. Update the Oozie config

a. Edit oozie-site.xml

    <property>
      <name>oozie.service.ProxyUserService.proxyuser.hue.hosts</name>
      <value>*</value>
    </property>
                  
    <property>
      <name>oozie.service.ProxyUserService.proxyuser.hue.groups</name>
      <value>*</value>
    </property>


5. Update HiveServer2 config

a. hive-site.xml

    <property>
      <name>hive.server2.enable.impersonation</name>
      <value>true</value>
    </property>

6. Provision Hue onto the servers

a. Within ansible run

    make provision-hue

7. Restart Namenode via Ambari and other services which need their configuration reloaded.



    ################################################################################
    #   Copyright 2015 Big Data Partnership Ltd                                    #
    #                                                                              #
    #   Licensed under the Apache License, Version 2.0 (the "License");            #
    #   you may not use this file except in compliance with the License.           #
    #   You may obtain a copy of the License at                                    #
    #                                                                              #
    #       http://www.apache.org/licenses/LICENSE-2.0                             #
    #                                                                              #
    #   Unless required by applicable law or agreed to in writing, software        #
    #   distributed under the License is distributed on an "AS IS" BASIS,          #
    #   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
    #   See the License for the specific language governing permissions and        #
    #   limitations under the License.                                             #
    ################################################################################
